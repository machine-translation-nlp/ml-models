#!/bin/bash
# Trains a model with Sockeye which supports source factoring
#
# Bash example: "./sockeye_const_training.sh train.en train.de sf_train.en sf_prepared valid.en valid.de sf_valid.en sf_model"
#
#
# Parameters: p_s_train_en    $1:   train data en
#             p_t_train_de    $2:   train data de  
#             p_sf_train_en   $3:   train data en with source factors
#             p_output        $4:   baseline model
#             v_data_en       $5:   valid data en
#             v_data_de       $6:   valid data de
#             v_sf_data_en    $7:   valid data en with source factors
#             m_output        $8:   model output
#
# Author: Ramón Wilhelm
# Date:   17/03/2022
#

p_s_train_en=$1
p_t_train_de=$2
p_sf_train_en=$3
p_output=$4
v_data_en=$5
v_data_de=$6
v_sf_data_en=$7
m_output=$8

sockeye-prepare-data \
    -s $p_s_train_en \
    -t $p_t_train_de --shared-vocab \
    -sf $p_sf_train_en \
    --source-factors-use-source-vocab true \
    --word-min-count 2 --pad-vocab-to-multiple-of 8 --max-seq-len 95 \
    --num-samples-per-shard 10000000 --output $p_output --max-processes $(nproc) 
	
sockeye-train -d $p_output \
-vs $v_data_en \
-vt $v_data_de \
--shared-vocab \
-o $m_output \
--overwrite-output \
--min-num-epochs 50 \
--max-num-epochs 100 \
--batch-size 560 \
--transformer-attention-heads 8:8 \
--transformer-activation-type 'relu':'relu' \
--transformer-dropout-act 0.1:0.1 \
--transformer-dropout-attention 0.1:0.1 \
--transformer-dropout-prepost 0.1:0.1 \
--transformer-feed-forward-num-hidden 2048:2048 \
--transformer-model-size 512 --num-layers 2:2 \
--transformer-positional-embedding-type fixed \
--transformer-preprocess n:n \
--transformer-postprocess dr:dr \
--source-factors-num-embed 1 \
--dtype float32 \
--max-seq-len 101:101 \
--num-words 32302:32302 \
--num-embed 512:512 \
--label-smoothing 0.1 \
--embed-dropout 0.0:0.0 \
--loss cross-entropy \
--keep-last-params 1 \
--cache-last-best-params 1 \
--validation-source-factors $v_sf_data_en

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#
# For Sockeye Source-Factors see here:
# https://awslabs.github.io/sockeye/inference.html#source-factors
#