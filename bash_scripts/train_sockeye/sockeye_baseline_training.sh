#!/bin/bash
# Trains a baseline model with Sockeye
#
# Bash example: "./sockeye_train_baseline.sh train.en train.de prepared valid.en valid.de baseline"
#
#
# Parameters: p_s_train_en    $1:   English file which will be translated 
#             p_t_train_de    $2:   German output file      
#             p_output        $3:   baseline model
#             v_data_en       $4:   valid data en
#             v_data_de       $5:   valid data de
#             m_output        $6:   model output
#
# Author: Ramón Wilhelm
# Date:   17/03/2022
#

p_s_train_en=$1
p_t_train_de=$2
p_output=$3
v_data_en=$4
v_data_de=$5
m_output=$6

sockeye-prepare-data \
    -s $p_s_train_en \
    -t $p_t_train_de --shared-vocab \
    --word-min-count 2 --pad-vocab-to-multiple-of 8 --max-seq-len 95 \
    --num-samples-per-shard 10000000 --output $p_output --max-processes $(nproc)  
	
sockeye-train -d $p_output \
-vs $v_data_en \
-vt $v_data_de \
--shared-vocab \
-o $m_output \
--overwrite-output \
--min-num-epochs 50 \
--max-num-epochs 100 \
--batch-size 560 \
--transformer-attention-heads 8:8 \
--transformer-activation-type 'relu':'relu' \
--transformer-dropout-act 0.1:0.1 \
--transformer-dropout-attention 0.1:0.1 \
--transformer-dropout-prepost 0.1:0.1 \
--transformer-feed-forward-num-hidden 2048:2048 \
--transformer-model-size 512 --num-layers 2:2 \
--transformer-positional-embedding-type fixed \
--transformer-preprocess n:n \
--transformer-postprocess dr:dr \
--dtype float32 \
--max-seq-len 101:101 \
--num-words 32302:32302 \
--num-embed 512:512 \
--label-smoothing 0.1 \
--embed-dropout 0.0:0.0 \
--loss cross-entropy \
--keep-last-params 1 \
--cache-last-best-params 1 \

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#