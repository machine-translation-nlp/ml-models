#!/bin/bash

#
# Based on the 'tokenize.sh' script by Raymond Susanto.
# Original script: https://github.com/raymondhs/constrained-levt/blob/master/tokenize.sh
#
# This script is a shortcut to tokenize and to apply BPE on an English raw text
# in order to translate it with Sockeye.
#
# The script downloads the 'mosesdecoder' and the 'subword-nmt'.
# Keep in mind to remove both folders when you do not need them afterwards.
#
# Parameters: input    $1:   bpe.codes
#					   $2:   text or file input
#
# Author: Ramón Wilhelm
# Date:   16/03/2022
#

l=en
bpe_code=$1

git clone https://github.com/moses-smt/mosesdecoder.git
git clone https://github.com/rsennrich/subword-nmt.git

SCRIPTS=mosesdecoder/scripts
TOKENIZER=$SCRIPTS/tokenizer/tokenizer.perl
NORM_PUNC=$SCRIPTS/tokenizer/normalize-punctuation.perl
REM_NON_PRINT_CHAR=$SCRIPTS/tokenizer/remove-non-printing-char.perl
BPEROOT=subword-nmt

perl $NORM_PUNC $l | \
perl $REM_NON_PRINT_CHAR | \
perl $TOKENIZER -threads 8 -a -l $l | \
python $BPEROOT/apply_bpe.py -c $bpe_code