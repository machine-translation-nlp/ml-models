#!/bin/bash
# Takes as input a raw English file and translates in the root of the directory and generates a translation without BPE.
#
# Bash example: "./levt_inference_file.sh input_levt_dataset_finalised_constraints.en output_levt_dataset_finalised_constraints.en data-bin/const_levt_en_de/ende.code data-bin/const_levt_en_de/checkpoint_best.pt"
#
# For constraint decoding the text lines in the file must contain tabulator delimiters with \t and with appending '|||' and constraints. 
# Example: The dog walked down the street.  |||Gasse"
#
# Parameters: input    $1:   English file which will be translated 
#             output   $2:   German output file
#			  bpecodes $3:   Bpe_codes for applying Bpe_codes
#			  model    $4:   Levenshtein model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#

input=$1
output=$2
bpecodes=$3
model=$4

cat $input \
| python interactive_with_constraints.py \
    data-bin/const_levt_en_de \
    -s en -t de \
    --task translation_lev \
    --path $model \
    --iter-decode-max-iter 9 \
    --iter-decode-eos-penalty 0 \
    --beam 1 \
    --tokenizer moses \
    --bpe fastbpe \
    --bpe-codes $bpecodes \
    --remove-bpe \
    --print-step \
    --batch-size 400 \
    --buffer-size 4000 \
    --preserve-constraint | tee /tmp/gen.out
  
cat /tmp/gen.out \
| grep ^H \
| sed 's/^H\-//' \
| sort -n -k 1 \
| cut -f 3 > $output
sed -ri 's/@@( |$)//g' $output

#
# For more information about lexically constraint decoding with Levenshtein see here: 
# https://github.com/raymondhs/constrained-levt
#