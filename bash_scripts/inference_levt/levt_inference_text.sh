#!/bin/bash
# Takes as input a raw English file and translates in the root of the directory and generates a translation without BPE.
#
# Bash example: "./levt_inference_text.sh "The dog walked down the street.\t|||Gasse" data-bin/const_levt_en_de/ende.code data-bin/const_levt_en_de/checkpoint_best.pt"
#
# For constraint decoding the text line must contain a constraint tabulator delimiter with \t and '|||'
# Example: The dog walked down the street.\t|||Gasse"
#
# Parameters: input    $1:   English file which will be translated 
#			  bpecodes $2:   Bpe_codes for applying Bpe_codes
#			  model    $3:   Levenshtein model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#
input=$1
bpecodes=$2
model=$3

echo -e $input \
| python interactive_with_constraints.py \
    data-bin/const_levt_en_de \
    -s en -t de \
    --task translation_lev \
    --path $model \
    --iter-decode-max-iter 9 \
    --iter-decode-eos-penalty 0 \
    --beam 1 \
    --tokenizer moses \
    --bpe fastbpe \
    --bpe-codes $bpecodes \
    --remove-bpe \
    --print-step \
    --batch-size 400 \
    --buffer-size 4000 \
    --preserve-constraint \
| grep ^H \
| sed 's/^H\-//' \
| sort -n -k 1 \
| cut -f 3 \
| sed -r 's/@@( |$)//g'

#
# For more information about lexically constraint decoding with Levenshtein see here: 
# https://github.com/raymondhs/constrained-levt
#