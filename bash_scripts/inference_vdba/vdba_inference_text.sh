#!/bin/bash
# Takes as input a raw English file and translate it to German file with a choosen
# beam size.
#
# You can choose between the two implementations 'ordered' and 'unordered'.
#
# Bash example: "./vdba_inference_text.sh "The dog walked down the street.\tGasse" 10 ordered bpecodes model1.pt"
#
# For constraint decoding the text must contain a tabulator delimiter \t with the constraint.
# Example: "The dog walked down the street.\tGasse"
#
# Parameters: input    			$1:   English text input 
#             beamsize			$2:   Recommended Beam Size between 5 and 10
#             implementation	$3:   "ordered" or "unordered"
#			  bpecodes			$4:   BPE-Codes for applying BPE
#			  model             $5:   fairseq-vdba model
#
#
# There are two implementations:
# * OrderedConstraintState: Tracks progress through an ordered list of multitoken 
# constraints.
#
# * UnorderedConstraintState: Tracks progress through an unordered list of multitoken 
# constraints.
#
# According to 'https://github.com/pytorch/fairseq/blob/main/fairseq/token_generation_constraints.py':
#
# The difference is that in the first, the constraints are assumed to be
# in order; the algorithm will permit zero or more tokens between them.
# In the second, the constraints are not ordered, so many orderings will
# be explored.
#
# The same sequence can be present any number of times, and will appear
# that many times in the output.
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#

input=$1
beamsize=$2
implementation=$3
bpecodes=$4
model=$5

echo -e $input \
| fairseq-interactive . \
  --path $model \
  --tokenizer moses \
  --bpe fastbpe \
  --bpe-codes $bpecodes \
  --remove-bpe \
  --constraints $implementation \
  -s en -t de \
  --beam $beamsize \
| grep ^H \
| sed 's/^H\-//' \
| sort -n -k 1 \
| cut -f 3 \
| sed -r 's/@@( |$)//g'

#
# For more information about lexically constraint decoding see here: 
# https://github.com/pytorch/fairseq/blob/main/examples/constrained_decoding/README.md
#