#!/bin/bash
# Take as input a raw English file and translates it to German file with a choosen
# beam size 
#
# You can choose between the two implementations 'ordered' and 'unordered'.
#
# Bash example: "./vdba_inference_file.sh input_vdba_dataset_finalised_constraints.en output_vdba_dataset_finalised_constraints.en 10 ordered bpecodes model1.pt"
#
# For constraint decoding the file must contain a tabulator delimiter \t
# Example: "The dog walked down the street.  Hund"
#
# Parameters: input    			$1:   English file which will be translated 
#             output			$2:   German output file
#             beamsize			$3:   Recommended Beam Size between 5 and 10
#             implementation	$4:   "ordered" or "unordered"
#			  bpecodes          $5:   bpecodes for applying bpe
#			  model             $6:   fairseq-vdba model
#
#
# There are two implementations:
# * OrderedConstraintState: Tracks progress through an ordered list of multitoken 
# constraints.
#
# * UnorderedConstraintState: Tracks progress through an unordered list of multitoken 
# constraints.
#
# According to 'https://github.com/pytorch/fairseq/blob/main/fairseq/token_generation_constraints.py':
#
# The difference is that in the first, the constraints are assumed to be
# in order; the algorithm will permit zero or more tokens between them.
# In the second, the constraints are not ordered, so many orderings will
# be explored.
#
# The same sequence can be present any number of times, and will appear
# that many times in the output.
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#

input=$1
output=$2
beamsize=$3
implementation=$4
bpecodes=$5
model=$6

cat $input\
| fairseq-interactive . \
  --path $model \
  --tokenizer moses \
  --bpe fastbpe \
  --bpe-codes $bpecodes \
  --remove-bpe \
  --constraints $implementation \
  -s en -t de \
  --beam $beamsize | tee /tmp/gen.out
  
cat /tmp/gen.out \
| grep ^H \
| sed 's/^H\-//' \
| sort -n -k 1 \
| cut -f 3 > $output
sed -ri 's/@@( |$)//g' $output

#
# For more information about lexically constraint decoding see here: 
# https://github.com/pytorch/fairseq/blob/main/examples/constrained_decoding/README.md
#