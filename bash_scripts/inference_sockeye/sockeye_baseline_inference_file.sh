#!/bin/bash
# Takes as inputs a tokenized English file with BPE and a corresponding file and 
# translates it with the Sockeye baseline model into a German file without BPE.
# It also generates as output a log file.
#
# Bash example: "./sockeye_baseline_inference_file.sh input_sockeye_bs_dataset_finalised_baseline.en output_sockeye_bs_dataset_finalised_baseline.en baseline"
#
# The file contains English lines like: "place legen the jack of hearts next to the ten of hearts ."
#
# 
# Parameters: input    $1:   English tokenized file with BPE which will be translated 
#             output   $2:   German output file
#             model    $3:   baseline model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#


input=$1
output=$2
model=$3

sockeye-translate \
    --input $input \
    --output $output \
    --model $model \
    --dtype float16 \
    --beam-size 5 \
    --batch-size 64
    
cat $output | sed -r 's/@@( |$)//g'

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#