#!/bin/bash
# Takes as input a tokenized English file with BPE including token-parallel source-factors 
# and translates it with the Sockeye terminology constraint model into a German file without BPE.
# It also generates as output a log file.
#
# Bash example: "./sockeye_constraints_inference_file_allinone.sh input_sockeye_bs_dataset_finalised_baseline_allinone.en output_sockeye_bs_dataset_finalised_baseline_allinone.en term_constraint_model"
#
# The file must contain text lines like
# "place|1 legen|2 the|0 jack|1 B@@|2 ub@@|2 en|2 of|0 hearts|1 Herz|2 next|0 to|0 the|0 ten|0 of|0 hearts|1 Herz|2 .|0"
#
# This file can be also used for baseline translation where the source factors are all 0.
#
# Parameters: input    $1:   English file which will be translated 
#             output   $2:   German output file
#             model    $3:   Transformer model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#


input=$1
output=$2
model=$3

cat $input \
  | sockeye-translate -m $model 2>/dev/null \
  | sed -r 's/@@( |$)//g' > $output

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#