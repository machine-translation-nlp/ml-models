#!/bin/bash
# Takes as input an English text including source-factors and translates it with the 
# Sockeye source-factoring model into a German file without BPE. 
#
# Bash example: "./sockeye_constraints_inference_text.sh "place|1 legen|2 the|0 jack|1 B@@|2 ub@@|2 en|2 of|0 hearts|1 Herz|2 next|0 to|0 the|0 ten|0 of|0 hearts|1 Herz|2 .|0" term_constraint_model"
#
# The text must contain a text line like
# "place|1 legen|2 the|0 jack|1 B@@|2 ub@@|2 en|2 of|0 hearts|1 Herz|2 next|0 to|0 the|0 ten|0 of|0 hearts|1 Herz|2 .|0"
#
# Parameters: input    $1:   English phrase which will be translated 
#             model    $2:   Transformer model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#

input=$1
model=$2

echo $input \
  | sockeye-translate -m $model 2>/dev/null \
  | sed -r 's/@@( |$)//g'

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#