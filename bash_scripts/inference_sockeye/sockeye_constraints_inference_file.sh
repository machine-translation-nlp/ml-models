#!/bin/bash
# Takes as inputs a tokenized English file with BPE and a corresponding file with token-parallel 
# source-factors and translates it with the Sockeye terminology model into a German file without BPE.
# It also generates as output a log file.
#
# Bash example: "./sockeye_constraints_inference_file.sh input_sockeye_bs_dataset_finalised_bpe.en input_sockeye_bs_dataset_finalised_bpe_sf.en output_sockeye_bs_dataset_finalised_bpe.en term_constraint_model"
#
# One file must contain English lines like: "place legen the jack B@@ ub@@ en of hearts Herz next to the ten of hearts Herz ."
#
# The second file contains the corresponding lines with token-parallel source factors: "1 2 0 1 2 2 2 0 1 2 0 0 0 0 0 1 2 0"
# 
# Parameters: input    		$1:   English file which will be translated 
#			  input source	$2:	  File with token-parallel source factors for the English input
#             output   		$3:   German output file
#             model    		$4:   Translation model
#
# Author: Ramón Wilhelm
# Date:   23/03/2022
#

input=$1
input_sf=$2
output=$3
model=$4

	
sockeye-translate \
    --input $input \
    --input-factors $input_sf \
    --output $output \
    --model $model \
    --dtype float16 \
    --beam-size 5 \
    --batch-size 64	
    
sed -ri 's/@@( |$)//g' $output

#
# For more information about Sockeye see here: 
# https://awslabs.github.io/sockeye/tutorials.html
#