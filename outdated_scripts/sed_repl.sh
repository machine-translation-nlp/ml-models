file=s_valid.en

sed -Ei 's/[0-9]/X/Ig' $file
sed -Ei 's/\<A@@ ce A@@ ss\>/1 1 2 2/Ig' $file
sed -Ei 's/\<A@@ ces A@@ sse\>/1 1 2 2/Ig' $file
sed -Ei 's/\<A@@ ces A@@ ssen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<A@@ ss A@@ ce\>/1 1 2 2/Ig' $file
sed -Ei 's/\<AB@@ IL@@ IF@@ Y AB@@ IL@@ IF@@ Y\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<AB@@ IL@@ IF@@ Y Ari@@ pi@@ pra@@ z@@ olt\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<Bi@@ pol@@ ar I Dis@@ order Bi@@ pol@@ ar @-@ I@@ - St@@ örung\>/1 1 1 1 1 1 2 2 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<Bor@@ gert Bor@@ gert\>/1 1 2 2/Ig' $file
sed -Ei 's/\<Bor@@ gert Le@@ wis@@ Mo@@ ss\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<Card Karte\>/1 2/Ig' $file
sed -Ei 's/\<Card Karten\>/1 2/Ig' $file
sed -Ei 's/\<Cards Karten\>/1 2/Ig' $file
sed -Ei 's/\<Cart Karte\>/1 2/Ig' $file
sed -Ei 's/\<Club Kreuz\>/1 2/Ig' $file
sed -Ei 's/\<David Kar@@ o\>/1 2 2/Ig' $file
sed -Ei 's/\<Diam@@ ond St@@ örung\>/1 1 2 2/Ig' $file
sed -Ei 's/\<Found@@ ations Fundament@@ st@@ apel\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<Foundation pi@@ le Fundament@@ st@@ apel\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<Foundation pil@@ es Fundament\>/1 1 1 2/Ig' $file
sed -Ei 's/\<Foundation Marcel\>/1 2/Ig' $file
sed -Ei 's/\<Hans Herz\>/1 2/Ig' $file
sed -Ei 's/\<Hear@@ t B@@ ub@@ en\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<J@@ acks Bu@@ be\>/1 1 2 2/Ig' $file
sed -Ei 's/\<Jack Jack\>/1 2/Ig' $file
sed -Ei 's/\<Jack James\>/1 2/Ig' $file
sed -Ei 's/\<James James\>/1 2/Ig' $file
sed -Ei 's/\<K@@ ings Kön@@ ige\>/1 1 2 2/Ig' $file
sed -Ei 's/\<K@@ lon@@ di@@ ke K@@ lon@@ di@@ ke\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<Kammer@@ er Davis\>/1 1 2/Ig' $file
sed -Ei 's/\<King König\>/1 2/Ig' $file
sed -Ei 's/\<Le@@ wis@@ Mo@@ ss Le@@ wis@@ Mo@@ ss\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<Matthe@@ w Matthe@@ w\>/1 1 2 2/Ig' $file
sed -Ei 's/\<Que@@ ens Dame\>/1 1 2/Ig' $file
sed -Ei 's/\<Que@@ ens Damen\>/1 1 2/Ig' $file
sed -Ei 's/\<Que@@ ens David\>/1 1 2/Ig' $file
sed -Ei 's/\<Re@@ deals Neu@@ vertei@@ lungen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<Reserve pil@@ es Reserv@@ est@@ apel\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<River River\>/1 2/Ig' $file
sed -Ei 's/\<Ro@@ gers Ro@@ gers\>/1 1 2 2/Ig' $file
sed -Ei 's/\<S@@ hel@@ f Dauer der Hal@@ t@@ barkeit\>/1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<Sp@@ ade Pi@@ k\>/1 1 2 2/Ig' $file
sed -Ei 's/\<Standard deck Standard @-@ Karten@@ satz\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<Stock Tal@@ on\>/1 2 2/Ig' $file
sed -Ei 's/\<Tab@@ le@@ au pil@@ es Tal@@ on\>/1 1 1 1 1 2 2/Ig' $file
sed -Ei 's/\<Tab@@ le@@ au Spiel@@ tisch\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<US@@ ER AN@@ W@@ EN@@ DER\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<W\. Jack\>/1 2/Ig' $file
sed -Ei 's/\<W\. James\>/1 2/Ig' $file
sed -Ei 's/\<W\. W\.\>/1 2/Ig' $file
sed -Ei 's/\<Wa@@ ste pi@@ le Rest@@ st@@ apel\>/1 1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<Wa@@ ste Ab@@ la@@ gest@@ apel\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<Wil@@ co@@ x Wil@@ co@@ x\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<absor@@ ption Res@@ or@@ ption\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<accompany begleiten\>/1 2/Ig' $file
sed -Ei 's/\<adjustment Do@@ si@@ san@@ passung\>/1 2 2 2 2/Ig' $file
sed -Ei 's/\<again neu\>/1 2/Ig' $file
sed -Ei 's/\<alcohol Alkohol\>/1 2/Ig' $file
sed -Ei 's/\<altern@@ ate wechsel@@ nder\>/1 1 2 2/Ig' $file
sed -Ei 's/\<anim@@ ate anim@@ iert\>/1 1 2 2/Ig' $file
sed -Ei 's/\<anti@@ psycho@@ tic therapy anti@@ psycho@@ tische Therapie\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<anti@@ psycho@@ tic anti@@ psycho@@ tische\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<ari@@ pi@@ pra@@ z@@ ole Ari@@ pi@@ pra@@ z@@ ol\>/1 1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<attempt Ver@@ such\>/1 2 2/Ig' $file
sed -Ei 's/\<base card Basi@@ sk@@ arte\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<base card Karte\>/1 1 2/Ig' $file
sed -Ei 's/\<be mark@@ eted in den Verkehr gebracht\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<bear River Be@@ ar River\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<bear Be@@ ar\>/1 2 2/Ig' $file
sed -Ei 's/\<behind zurück\>/1 2/Ig' $file
sed -Ei 's/\<betting behalten\>/1 2/Ig' $file
sed -Ei 's/\<bird Sp@@ atz\>/1 2 2/Ig' $file
sed -Ei 's/\<blood glu@@ co@@ se fluctu@@ ation Blut@@ zu@@ ck@@ ersch@@ wan@@ kung\>/1 1 1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<blood glu@@ co@@ se Blut@@ zu@@ cker\>/1 1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<blood pressure Blut@@ druck\>/1 1 2 2/Ig' $file
sed -Ei 's/\<blood Blut\>/1 2/Ig' $file
sed -Ei 's/\<bottom Fuß@@ zeile\>/1 2 2/Ig' $file
sed -Ei 's/\<bug Fehler\>/1 2/Ig' $file
sed -Ei 's/\<build aufbauen\>/1 2/Ig' $file
sed -Ei 's/\<build legen\>/1 2/Ig' $file
sed -Ei 's/\<built up aufgebaut\>/1 1 2/Ig' $file
sed -Ei 's/\<built aufgebaut\>/1 2/Ig' $file
sed -Ei 's/\<built gebaut\>/1 2/Ig' $file
sed -Ei 's/\<bury be@@ graben\>/1 2 2/Ig' $file
sed -Ei 's/\<by nach\>/1 2/Ig' $file
sed -Ei 's/\<card Karte\>/1 2/Ig' $file
sed -Ei 's/\<card Karten\>/1 2/Ig' $file
sed -Ei 's/\<card karte\>/1 2/Ig' $file
sed -Ei 's/\<cards Karte\>/1 2/Ig' $file
sed -Ei 's/\<cards Karten\>/1 2/Ig' $file
sed -Ei 's/\<cards Spiel@@ karte\>/1 2 2/Ig' $file
sed -Ei 's/\<cards Spiel@@ karte\>/1 2 2/Ig' $file
sed -Ei 's/\<cards karten\>/1 2/Ig' $file
sed -Ei 's/\<cart Karte\>/1 2/Ig' $file
sed -Ei 's/\<challenge Ziel\>/1 2/Ig' $file
sed -Ei 's/\<children Kinder\>/1 2/Ig' $file
sed -Ei 's/\<commodity Gut\>/1 2/Ig' $file
sed -Ei 's/\<completed suicide began@@ gener Su@@ izi@@ d\>/1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<completed began@@ gener\>/1 2 2/Ig' $file
sed -Ei 's/\<concent@@ rations Konzent@@ rationen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<concent@@ rations er@@ wo@@ gen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<considered er@@ wo@@ gen\>/1 2 2 2/Ig' $file
sed -Ei 's/\<contains enthält\>/1 2/Ig' $file
sed -Ei 's/\<continuation Aufrechterhaltung\>/1 2/Ig' $file
sed -Ei 's/\<conventional konventionellen\>/1 2/Ig' $file
sed -Ei 's/\<corner slot Eck@@ feld\>/1 1 2 2/Ig' $file
sed -Ei 's/\<cre@@ at@@ ine pho@@ sp@@ ho@@ kin@@ ase Kre@@ at@@ in@@ pho@@ sp@@ ho@@ kin@@ ase\>/1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<cross gehen\>/1 2/Ig' $file
sed -Ei 's/\<de@@ men@@ tia @-@ related Dem@@ enz @-@ assozi@@ ierter\>/1 1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<de@@ men@@ tia Dem@@ enz\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<deal Verteilung\>/1 2/Ig' $file
sed -Ei 's/\<deal geben\>/1 2/Ig' $file
sed -Ei 's/\<deal geben Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<deal hat\>/1 2/Ig' $file
sed -Ei 's/\<dealing Geben\>/1 2/Ig' $file
sed -Ei 's/\<dealing Geben Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<dealing abgelegt\>/1 2/Ig' $file
sed -Ei 's/\<deals aufge@@ deckt\>/1 2 2/Ig' $file
sed -Ei 's/\<dealt gelegt\>/1 2/Ig' $file
sed -Ei 's/\<dealt zu tun\>/1 2 2/Ig' $file
sed -Ei 's/\<dealt zu tun haben\>/1 2 2 2/Ig' $file
sed -Ei 's/\<deck of cards Satz aus Karten\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<deck St@@ apel\>/1 2 2/Ig' $file
sed -Ei 's/\<demonstrate zeigte\>/1 2/Ig' $file
sed -Ei 's/\<dental flo@@ ss Zahn@@ sei@@ de\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<deposit abzu@@ legen\>/1 2 2/Ig' $file
sed -Ei 's/\<det@@ ec@@ table abhängi@@ gen\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<diam@@ ond Kar@@ o\>/1 1 2 2/Ig' $file
sed -Ei 's/\<diam@@ ond Kar@@ o @-@ Karte\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<diam@@ onds Diam@@ anten\>/1 1 2 2/Ig' $file
sed -Ei 's/\<diam@@ onds Herzen\>/1 1 2/Ig' $file
sed -Ei 's/\<diam@@ onds Kar@@ o @-@ Karten\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<diam@@ onds Kar@@ os\>/1 1 2 2/Ig' $file
sed -Ei 's/\<directions Anweisungen\>/1 2/Ig' $file
sed -Ei 's/\<disorders Er@@ krank@@ ungen\>/1 2 2 2/Ig' $file
sed -Ei 's/\<disorders stör@@ ung\>/1 2 2/Ig' $file
sed -Ei 's/\<disposal Beseitigung\>/1 2/Ig' $file
sed -Ei 's/\<distribute verbreiten\>/1 2/Ig' $file
sed -Ei 's/\<dose Do@@ sis\>/1 2 2/Ig' $file
sed -Ei 's/\<double click klicken Sie doppelt\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<double clicking Doppel@@ klick\>/1 1 2 2/Ig' $file
sed -Ei 's/\<down ab@@ stei@@ gend\>/1 2 2 2/Ig' $file
sed -Ei 's/\<drag ziehen\>/1 2/Ig' $file
sed -Ei 's/\<edge slot Rand@@ feld\>/1 1 2 2/Ig' $file
sed -Ei 's/\<effect Effekt\>/1 2/Ig' $file
sed -Ei 's/\<effectiveness Wirksamkeit\>/1 2/Ig' $file
sed -Ei 's/\<effects Anwendung\>/1 2/Ig' $file
sed -Ei 's/\<effects Auswirkung\>/1 2/Ig' $file
sed -Ei 's/\<effects Effekte\>/1 2/Ig' $file
sed -Ei 's/\<effects wirkungen\>/1 2/Ig' $file
sed -Ei 's/\<eight of hearts Herz @-@ Acht\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<eight Acht\>/1 2/Ig' $file
sed -Ei 's/\<eight Vier\>/1 2/Ig' $file
sed -Ei 's/\<elderly ältere\>/1 2/Ig' $file
sed -Ei 's/\<elev@@ ated Erhöhung\>/1 1 2/Ig' $file
sed -Ei 's/\<empty pil@@ es le@@ ere St@@ apel\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<evaluated Untersuchungen\>/1 2/Ig' $file
sed -Ei 's/\<evaluation Untersuchungen\>/1 2/Ig' $file
sed -Ei 's/\<ex@@ cip@@ i@@ ents Bestandteile\>/1 1 1 1 2/Ig' $file
sed -Ei 's/\<exposure Ex@@ positionen\>/1 2 2/Ig' $file
sed -Ei 's/\<face value Punk@@ t@@ wert\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<filled belegt\>/1 2/Ig' $file
sed -Ei 's/\<fishing wire Angel@@ sch@@ nur\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<fishing Angel\>/1 2/Ig' $file
sed -Ei 's/\<five of hearts Herz @-@ Fünf\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<five Fünf\>/1 2/Ig' $file
sed -Ei 's/\<fli@@ pped aufge@@ deckt\>/1 1 2 2/Ig' $file
sed -Ei 's/\<for nach\>/1 2/Ig' $file
sed -Ei 's/\<form zusammen@@ zustellen\>/1 2 2/Ig' $file
sed -Ei 's/\<formation Anordnung\>/1 2/Ig' $file
sed -Ei 's/\<formation Formation\>/1 2/Ig' $file
sed -Ei 's/\<foundation Fundament\>/1 2/Ig' $file
sed -Ei 's/\<foundation Fundament@@ st@@ apel\>/1 2 2 2/Ig' $file
sed -Ei 's/\<foundations Fundament\>/1 2/Ig' $file
sed -Ei 's/\<foundations Fundament@@ st@@ apel\>/1 2 2 2/Ig' $file
sed -Ei 's/\<four Acht\>/1 2/Ig' $file
sed -Ei 's/\<four Vier\>/1 2/Ig' $file
sed -Ei 's/\<go arbeiten\>/1 2/Ig' $file
sed -Ei 's/\<go arbeiten Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<group of cards Karten@@ gruppe\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<group of cards Spiel@@ karte@@ gruppe\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<group Gruppe\>/1 2/Ig' $file
sed -Ei 's/\<groups Gruppen\>/1 2/Ig' $file
sed -Ei 's/\<handling Handhabung\>/1 2/Ig' $file
sed -Ei 's/\<hands Hände\>/1 2/Ig' $file
sed -Ei 's/\<harm schaden\>/1 2/Ig' $file
sed -Ei 's/\<have sor@@ tieren\>/1 2 2/Ig' $file
sed -Ei 's/\<haz@@ ard Gefahren\>/1 1 2/Ig' $file
sed -Ei 's/\<hearts Herz\>/1 2/Ig' $file
sed -Ei 's/\<hearts Herzen\>/1 2/Ig' $file
sed -Ei 's/\<hin@@ t Ti@@ pp\>/1 1 2 2/Ig' $file
sed -Ei 's/\<hyper@@ sensitivity Über@@ empfin@@ dlichkeit\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<hypersensitivity Über@@ empfin@@ dlichkeit\>/1 2 2 2/Ig' $file
sed -Ei 's/\<implement@@ ations Imple@@ ment@@ ationen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<increase verstärken\>/1 2/Ig' $file
sed -Ei 's/\<ingredients Arzneimittel\>/1 2/Ig' $file
sed -Ei 's/\<ingredients Inhalts@@ stoffe\>/1 2 2/Ig' $file
sed -Ei 's/\<initial initial\>/1 2/Ig' $file
sed -Ei 's/\<intoler@@ ance Un@@ verträ@@ glichkeit\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<involvement beteiligung\>/1 2/Ig' $file
sed -Ei 's/\<involving über\>/1 2/Ig' $file
sed -Ei 's/\<jack of diam@@ onds Kar@@ o @-@ B@@ ub@@ en\>/1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<jack of hearts Herz @-@ B@@ ub@@ en\>/1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<jack B@@ ub@@ e\>/1 2 2 2/Ig' $file
sed -Ei 's/\<jack Bauer\>/1 2/Ig' $file
sed -Ei 's/\<jack Jack\>/1 2/Ig' $file
sed -Ei 's/\<keep auf@@ bewahren\>/1 2 2/Ig' $file
sed -Ei 's/\<king of diam@@ onds Kar@@ o @-@ König\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<king of hearts Herz @-@ König\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<king König\>/1 2/Ig' $file
sed -Ei 's/\<lag@@ ging zu@@ stehen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<life Hal@@ t@@ barkeit\>/1 2 2 2/Ig' $file
sed -Ei 's/\<limits Grenz@@ werten\>/1 2 2/Ig' $file
sed -Ei 's/\<loss Verlust\>/1 2/Ig' $file
sed -Ei 's/\<lower niedriger\>/1 2/Ig' $file
sed -Ei 's/\<m@@ yo@@ glob@@ inu@@ ria M@@ yo@@ glob@@ inu@@ ri@@ e\>/1 1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<maintenance of effect Er@@ halt@@ ung@@ se@@ ffekt\>/1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<making a total zusammen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<manu@@ als Hand@@ bü@@ chern\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<manual Handbuch\>/1 2/Ig' $file
sed -Ei 's/\<match kombinieren\>/1 2/Ig' $file
sed -Ei 's/\<maximum daily dose Maxim@@ al@@ do@@ sis\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<maximum maximal\>/1 2/Ig' $file
sed -Ei 's/\<medical treatment ärz@@ tlichen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<medical ärz@@ tlichen\>/1 2 2/Ig' $file
sed -Ei 's/\<medicinal products Arznei@@ mittel\>/1 1 2 2/Ig' $file
sed -Ei 's/\<medicinal Arznei\>/1 2/Ig' $file
sed -Ei 's/\<medicines Arzneimittel\>/1 2/Ig' $file
sed -Ei 's/\<medicines Über@@ empfin@@ dlichkeit\>/1 2 2 2/Ig' $file
sed -Ei 's/\<met@@ abo@@ lic path@@ way Ver@@ stoff@@ wechsel@@ ung\>/1 1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<met@@ abo@@ lic path@@ way Über@@ empfin@@ dlichkeit\>/1 1 1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<mind Hinter@@ kopf\>/1 2 2/Ig' $file
sed -Ei 's/\<mini stroke vorüber@@ gehender Man@@ gel@@ durch@@ bl@@ utung des Gehir@@ ns\>/1 1 2 2 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<mo@@ ist@@ ure Feu@@ chtigkeit\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<mono@@ therapy Mono@@ therapie\>/1 1 2 2/Ig' $file
sed -Ei 's/\<move Zug\>/1 2/Ig' $file
sed -Ei 's/\<move legen Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<move nehmen Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<move verschieben\>/1 2/Ig' $file
sed -Ei 's/\<move verschieben Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<move wechseln\>/1 2/Ig' $file
sed -Ei 's/\<moved gelegt\>/1 2/Ig' $file
sed -Ei 's/\<moved gelegte\>/1 2/Ig' $file
sed -Ei 's/\<moved verschoben\>/1 2/Ig' $file
sed -Ei 's/\<moved wechseln\>/1 2/Ig' $file
sed -Ei 's/\<movement Bewegungs@@ freiheit\>/1 2 2/Ig' $file
sed -Ei 's/\<multiple medicinal product involvement multi@@ pler Arznei@@ mittel@@ beteiligung\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<ner@@ v@@ ousness Ner@@ vo@@ si@@ tät\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<next neben\>/1 2/Ig' $file
sed -Ei 's/\<occurred traten\>/1 2/Ig' $file
sed -Ei 's/\<on zur\>/1 2/Ig' $file
sed -Ei 's/\<only lediglich\>/1 2/Ig' $file
sed -Ei 's/\<onto auf\>/1 2/Ig' $file
sed -Ei 's/\<original Card Karte\>/1 1 2/Ig' $file
sed -Ei 's/\<original package Original@@ verpack@@ ung\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<original original\>/1 2/Ig' $file
sed -Ei 's/\<out zugänglich\>/1 2/Ig' $file
sed -Ei 's/\<over verloren\>/1 2/Ig' $file
sed -Ei 's/\<pa@@ irs pa@@ ar@@ weise\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<pack sizes Pack@@ ungs@@ größen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<pack Pack@@ ungs@@ en\>/1 2 2 2/Ig' $file
sed -Ei 's/\<package le@@ af@@ let Pack@@ ungs@@ bei@@ lage\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<package verpack@@ ung\>/1 2 2/Ig' $file
sed -Ei 's/\<patient groups Gruppen\>/1 1 2/Ig' $file
sed -Ei 's/\<patient groups Pati@@ ent@@ engruppen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<patient Pati@@ ent\>/1 2 2/Ig' $file
sed -Ei 's/\<patients Patienten\>/1 2/Ig' $file
sed -Ei 's/\<pharmac@@ ok@@ ine@@ tic pharma@@ kok@@ in@@ eti@@ schen\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<pharmac@@ ok@@ ine@@ tics Pharma@@ kok@@ in@@ eti@@ k\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<pi@@ le in the Tab@@ le@@ au Tisch@@ st@@ apel\>/1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<pi@@ le Ab@@ lage\>/1 1 2 2/Ig' $file
sed -Ei 's/\<pil@@ es Ab@@ lagen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<plac@@ eb@@ o Pl@@ ac@@ eb@@ o\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<place auslegen\>/1 2/Ig' $file
sed -Ei 's/\<place legen\>/1 2/Ig' $file
sed -Ei 's/\<place legen Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<placing Ab@@ legen\>/1 2 2/Ig' $file
sed -Ei 's/\<placing ab@@ legen\>/1 2 2/Ig' $file
sed -Ei 's/\<play Spiel\>/1 2/Ig' $file
sed -Ei 's/\<playing bewegen\>/1 2/Ig' $file
sed -Ei 's/\<point Punkt\>/1 2/Ig' $file
sed -Ei 's/\<poker hands Poker @-@ Hände\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<poker Poker\>/1 2/Ig' $file
sed -Ei 's/\<possible erzi@@ el@@ bare\>/1 2 2 2/Ig' $file
sed -Ei 's/\<precau@@ tions Vor@@ sichts@@ maßnahmen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<pres@@ cription Versch@@ reib@@ ungsp@@ fli@@ chtig\>/1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<preventing Prävention\>/1 2/Ig' $file
sed -Ei 's/\<product mittel\>/1 2/Ig' $file
sed -Ei 's/\<propor@@ tions of patients Pati@@ ent@@ engruppen\>/1 1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<proposals Vorschläge\>/1 2/Ig' $file
sed -Ei 's/\<protect schützen\>/1 2/Ig' $file
sed -Ei 's/\<psycho@@ sis Psy@@ chose\>/1 1 2 2/Ig' $file
sed -Ei 's/\<rank Ran@@ ges\>/1 2 2/Ig' $file
sed -Ei 's/\<re@@ arran@@ ging zusammen@@ zustellen\>/1 1 1 2 2/Ig' $file
sed -Ei 's/\<re@@ deal neu@@ zuver@@ teilen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<re@@ deals Neu@@ vertei@@ lungen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<reach un@@ zugänglich\>/1 2 2/Ig' $file
sed -Ei 's/\<read beachten\>/1 2/Ig' $file
sed -Ei 's/\<recur@@ rence Rück@@ falls\>/1 1 2 2/Ig' $file
sed -Ei 's/\<regi@@ cide Herr@@ sch@@ ermor@@ d\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<relevance Bedeutung\>/1 2/Ig' $file
sed -Ei 's/\<remove verschieben Sie\>/1 2 2/Ig' $file
sed -Ei 's/\<removed entfer@@ nte\>/1 2 2/Ig' $file
sed -Ei 's/\<reproduction Re@@ produk@@ tion@@ sto@@ x@@ iz@@ ität\>/1 2 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<reserve slot Reserv@@ est@@ apel\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<reserve Reserv@@ est@@ apel\>/1 2 2 2/Ig' $file
sed -Ei 's/\<response angesprochen\>/1 2/Ig' $file
sed -Ei 's/\<reveal decken\>/1 2/Ig' $file
sed -Ei 's/\<revealed erg@@ ab\>/1 2 2/Ig' $file
sed -Ei 's/\<revealed ließen\>/1 2/Ig' $file
sed -Ei 's/\<right Rechten\>/1 2/Ig' $file
sed -Ei 's/\<row Reihe\>/1 2/Ig' $file
sed -Ei 's/\<s@@ wal@@ low schlucken\>/1 1 1 2/Ig' $file
sed -Ei 's/\<sc@@ oring Punk@@ t@@ wertung\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<schi@@ z@@ op@@ hr@@ en@@ ia Sch@@ iz@@ op@@ hr@@ en@@ ie\>/1 1 1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<schi@@ z@@ op@@ hr@@ en@@ ic schi@@ z@@ op@@ hr@@ ener\>/1 1 1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<score Punkte\>/1 2/Ig' $file
sed -Ei 's/\<scores erzielt\>/1 2/Ig' $file
sed -Ei 's/\<scores zählt\>/1 2/Ig' $file
sed -Ei 's/\<searching suchen\>/1 2/Ig' $file
sed -Ei 's/\<seems scheint\>/1 2/Ig' $file
sed -Ei 's/\<sequ@@ ences Ab@@ folgen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<sequence Rang\>/1 2/Ig' $file
sed -Ei 's/\<sets Reihe\>/1 2/Ig' $file
sed -Ei 's/\<seven of sp@@ ades Pi@@ k @-@ Si@@ eben\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<severe schwerer\>/1 2/Ig' $file
sed -Ei 's/\<sh@@ u@@ ff@@ le M@@ ischen\>/1 1 1 1 2 2/Ig' $file
sed -Ei 's/\<side effects Neben@@ wirkungen\>/1 1 2 2/Ig' $file
sed -Ei 's/\<side Neben@@\>/1 2/Ig' $file
sed -Ei 's/\<signs Symptom@@ e\>/1 2 2/Ig' $file
sed -Ei 's/\<six of sp@@ ades Pi@@ k @-@ Se@@ chs\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<six Se@@ chs\>/1 2 2/Ig' $file
sed -Ei 's/\<slot Feld\>/1 2/Ig' $file
sed -Ei 's/\<slot Platz\>/1 2/Ig' $file
sed -Ei 's/\<smo@@ kers Rau@@ cher\>/1 1 2 2/Ig' $file
sed -Ei 's/\<smoking Rau@@ cher\>/1 2 2/Ig' $file
sed -Ei 's/\<solution for injection In@@ jek@@ tions@@ lösung\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<solution lösung\>/1 2/Ig' $file
sed -Ei 's/\<sp@@ ade Pi@@ k\>/1 1 2 2/Ig' $file
sed -Ei 's/\<sp@@ ades Pi@@ k\>/1 1 2 2/Ig' $file
sed -Ei 's/\<space Lücke\>/1 2/Ig' $file
sed -Ei 's/\<space Platz\>/1 2/Ig' $file
sed -Ei 's/\<space Wel@@ traum\>/1 2 2/Ig' $file
sed -Ei 's/\<spaces Mittel@@ plätze\>/1 2 2/Ig' $file
sed -Ei 's/\<spaces Plätze\>/1 2/Ig' $file
sed -Ei 's/\<store auf@@ bewahren\>/1 2 2/Ig' $file
sed -Ei 's/\<stroke Man@@ gel@@ durch@@ bl@@ utung des Gehir@@ ns\>/1 2 2 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<stroke Schla@@ gan@@ fall\>/1 2 2 2/Ig' $file
sed -Ei 's/\<stroke Schla@@ gan@@ fällen\>/1 2 2 2/Ig' $file
sed -Ei 's/\<studies Studien\>/1 2/Ig' $file
sed -Ei 's/\<subjects Pro@@ ban@@ den\>/1 2 2 2/Ig' $file
sed -Ei 's/\<suici@@ dal ide@@ ation Su@@ izi@@ d@@ ge@@ danken\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<suicide attempt Su@@ izi@@ d@@ ver@@ such\>/1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<suicide Su@@ izi@@ d@@\>/1 2 2 2/Ig' $file
sed -Ei 's/\<suit Farbe\>/1 2/Ig' $file
sed -Ei 's/\<sul@@ ph@@ ate con@@ ju@@ gates Sul@@ fat @-@ Kon@@ ju@@ gate\>/1 1 1 1 1 1 2 2 2 2 2 2/Ig' $file
sed -Ei 's/\<superior über@@ le@@ gene\>/1 2 2 2/Ig' $file
sed -Ei 's/\<supervision Überwachung\>/1 2/Ig' $file
sed -Ei 's/\<symptoms Beschwerden\>/1 2/Ig' $file
sed -Ei 's/\<tab@@ le@@ au slot Spiel@@ ti@@ sch@@ platz\>/1 1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<tab@@ let Tab@@ lette\>/1 1 2 2/Ig' $file
sed -Ei 's/\<ten of diam@@ onds Kar@@ o @-@ Zeh@@ n\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<ten of hearts Herz @-@ Zeh@@ n\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<ten Zeh@@ n\>/1 2 2/Ig' $file
sed -Ei 's/\<themes mo@@ tive\>/1 2 2/Ig' $file
sed -Ei 's/\<therapy Therapie\>/1 2/Ig' $file
sed -Ei 's/\<there es\>/1 2/Ig' $file
sed -Ei 's/\<there es gibt\>/1 2 2/Ig' $file
sed -Ei 's/\<tool@@ bar Werk@@ zeu@@ glei@@ ste\>/1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<top oberste\>/1 2/Ig' $file
sed -Ei 's/\<tox@@ ic@@ ation Arznei@@ mittel@@ ver@@ gi@@ ftung\>/1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<tox@@ ic@@ ity To@@ x@@ iz@@ ität\>/1 1 1 2 2 2 2/Ig' $file
sed -Ei 's/\<trails Studien\>/1 2/Ig' $file
sed -Ei 's/\<treated behandelt\>/1 2/Ig' $file
sed -Ei 's/\<treatment Behandlung\>/1 2/Ig' $file
sed -Ei 's/\<trial Studie\>/1 2/Ig' $file
sed -Ei 's/\<try ver@@ suchen\>/1 2 2/Ig' $file
sed -Ei 's/\<try ver@@ suchen Sie\>/1 2 2 2/Ig' $file
sed -Ei 's/\<turned over aufge@@ deckt\>/1 2 2 2/Ig' $file
sed -Ei 's/\<two in the bus@@ h Tau@@ be auf dem Dach\>/1 1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<two Zwei\>/1 2/Ig' $file
sed -Ei 's/\<undo machen Sie rück@@ gängig\>/1 2 2 2 2/Ig' $file
sed -Ei 's/\<undo machen rück@@ gängig\>/1 2 2 2/Ig' $file
sed -Ei 's/\<undo rück@@ gängig\>/1 2 2/Ig' $file
sed -Ei 's/\<up auf\>/1 2/Ig' $file
sed -Ei 's/\<v@@ as@@ cular disorders Gef@@ ä@@ ßer@@ krank@@ ungen\>/1 1 1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<v@@ as@@ cular Gef@@ ä@@ ß\>/1 1 1 2 2 2/Ig' $file
sed -Ei 's/\<vi@@ al Durch@@ ste@@ ch@@ fla@@ sche\>/1 1 2 2 2 2 2/Ig' $file
sed -Ei 's/\<vi@@ al Fla@@ sche\>/1 1 2 2/Ig' $file
sed -Ei 's/\<waste Rest@@ est@@ ap@@ els\>/1 2 2 2 2/Ig' $file
sed -Ei 's/\<waste Zeu@@ gs\>/1 2 2/Ig' $file
sed -Ei 's/\<whole un@@ z@@ erk@@ aut\>/1 2 2 2 2/Ig' $file
sed -Ei 's/\<wire Sch@@ nur\>/1 2 2/Ig' $file
sed -Ei 's/\<wra@@ pping erfolgt\>/1 1 2/Ig' $file
sed -Ei 's/\<wra@@ pping ges@@ pr@@ ungen\>/1 1 2 2 2/Ig' $file
sed -Ei 's/\<(1)\>|\<(2)\>/\n&/g; s/(^| )\S+/\10/g; s/\n//g' $file
sed -Ei 's/\<(0)\>|\<(2)\>/\n&/g; s/(^| )\S+/\11/g; s/\n//g' $file
sed -Ei 's/\<(1)\>|\<(0)\>/\n&/g; s/(^| )\S+/\12/g; s/\n//g' $file