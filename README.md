Machine Translation Models - Terminology Constraints and Lexically Constrained Decoding with Vectorized Dynamic Beam Allocation and Levenshtein Transformer
====================

This is a repository where the important programming files replicating the models from the following paper is kept:

- [Lexically Constrained Neural Machine Translation with Levenshtein Transformer (Susanto et al., 2020)](Lexically Constrained Neural Machine Translation with Levenshtein Transformer)
- [Improved Lexically Constrained Decoding for Translation and Monolingual Rewriting (Hu et al., 2019)](https://aclanthology.org/N19-1090/)
- [Training Neural Machine Translation To Apply Terminology Constraints (Dinu et al., 2019)](https://arxiv.org/abs/1906.01105)

With these programming files, the start to recreate the models is facilitated and it can be changed as needed.
The following transformer models (descending order) are called _Levenshtein_, _Vectorized Dynamic Beam Allocation (VDBA)_ and _Terminology Constraints_.

In the table index you will find the instructions to replicate the respective models.

The toolkits [AWS Sockeye](https://github.com/awslabs/sockeye) and [FairSeq](https://github.com/pytorch/fairseq) are used for the replicas (as of 09.01.2022).
Please, read the _README.md_ carefully before moving on.

* [Requirements](#markdown-header-requirements)
	* [System Requirements](#markdown-header-system-requirements)
	* [Virtual Environments](#markdown-header-virtual-environments)
	* [Installing Requirements](#markdown-header-installing-requirements)
* [Datasets](#markdown-header-datasets)
	* [Bash Script](#markdown-header-bash-script)
	* [Manually Preprocessing](#markdown-header-manually-preprocessing)
	* [Preprocessing Notes](#markdown-header-preprocessing-notes)
* [Levenshtein](#markdown-header-levenshtein)
	* [Translate Levenshtein](#markdown-header-translate-levenshtein)
	* [Train Levenshtein](#markdown-header-levenshtein)
* [Vectorized Dynamic Beam Allocation](#markdown-header-vectorized-dynamic-beam-allocation)
	* [Translate with Vectorized Dynamic Beam Allocation](#markdown-header-translate-with-vectorized-dynamic-beam-allocation)
	* [Train Vectorized Dynamic Beam Allocation](#markdown-header-train-vectorized-dynamic-beam-allocation)
* [Terminology Constraints](#markdown-header-terminology-constraints)
	* [Translate Terminology Constraint](#markdown-header-translate-terminology-constraint)
	* [Train Terminology Constraint Model](#markdown-header-train-terminology-constraint-model)
* [Evaluation](#markdown-header-evaluation)
* [Updates](#markdown-header-updates)
* [Notes](#markdown-header-notes)
* [Further Reading](#markdown-header-further-reading)
* [References](#markdown-header-references)
	
# Requirements

To use the FairSeq and Sockeye toolkits, you must ensure that your system meets the following hardware requirements

## System Requirements

- (NVIDIA) GPU 10.2
- Python 3.7
- PyTorch 1.10
- Numpy

## Virtual Environments

Since it leads to problems especially when all three models are to be replicated at the same time, it is advisable to create a separate virtual environment for each model beforehand.
[Anaconda](https://www.anaconda.com/) is best suited to create Virtual Environments with _Python 3.7_.


`conda create -n yourenvname python=3.7 anaconda`

Alternatively, a Virtual Environment can be created with python.

```
	pip install virtualenv
	python -m virtualenv yourenvname
	source yourenvname/bin/activate
```

__Note:__ if you did not worked in a virtual environment, you have to re-install some requirenments e.g. FairSeq.

## Installing Requirements

All the important components needed for PyTorch, Sockeye, Fairseq and so on can be simply installed with

`pip install -r requirements.txt --no-index`

in the root folder.

For the installation of FairSeq you have to decide, if you want to replicate the Levenshtein model first or the transformer model for Vectorized Dynamic Beam Allocation (see the instructions for the respective models).

# Datasets

## Bash Script

The _get_de_en_dataset.sh_ file in the folder _dataset_ contains a bash script to download and preprocess the [WMT'14 English to German (Convolutional)](https://github.com/pytorch/fairseq/tree/main/examples/translation#wmt14-english-to-german-convolutional) dataset from the official [Fairseq](https://github.com/pytorch/fairseq) repository.
In this script, the data will be tokenized with the [Moses Tokenizer](https://github.com/moses-smt/mosesdecoder) and preprocessed in subword units via [Byte-Pair-Encoding (BPE)](https://github.com/rsennrich/subword-nmt).

You execute the bash script with

`` bash dataset/get_de_en_dataset.sh``

If you want to use and preprocess other datasets feel free to edit the download links in the _get_de_en_dataset.sh_ file.

You can also preprocess the data manually.

## Manually Preprocessing

If you do not want to use the bash script you also preprocess the data manually.
Assuming you use the _europarl.v7.de-en_ dataset.
1. You first need to clone the latest Moses tokenizer and Subword NMT.


```
	git clone https://github.com/moses-smt/mosesdecoder.git
	git clone https://github.com/rsennrich/subword-nmt.git
```

2. To tokenize the German data, you have to execute these commands in a pipe. Same goes for the test set.


```
	cat europarl-v7.de-en.de | \
		perl mosesdecoder/scripts/tokenizer/normalize-punctuation.perl de | \
		perl mosesdecoder/scripts/tokenizer/remove-non-printing-char.perl | \
		perl mosesdecoder/scripts/tokenizer/tokenizer.perl -threads 8 -a -l de >> europarl_temp/train.tags.de-en.tok.de
```

__Note:__ Normalization is necessary to standardize special characters like quotation marks. Characters that cannot be printed should also be removed.

3. To split _train_ and _valid_ data use these commands.

```
    awk '{if (NR%100 == 0)  print $0; }' train.tags.de-en.tok.de > valid.de
    awk '{if (NR%100 != 0)  print $0; }' train.tags.de-en.tok.de > train.de
```

4. We need to subword all the tokens to 40000 subunits and learn the BPE on the _train_, _test_ and _valid_ data.

```
	train.* >> train.tmp
	python subword-nmt/subword_nmt/learn_bpe.py -s 40000 < train.tmp > bpe.code
	
	python subword-nmt/subword_nmt/apply_bpe.py -c bpe.code < train.de-en.de > bpe.train.de
```

5. At last we need to clean the corpus of the corpus of the train and valid data.

```
	perl mosesdecoder/scripts/training/clean-corpus-n.perl -ratio 1.5 bpe.train de en train 1 250
	perl mosesdecoder/scripts/training/clean-corpus-n.perl -ratio 1.5 bpe.valid de en valid 1 250
```

6. Rename the bpe.test.${lang} files to test.${lang}.

7. Congratulations! Your dataset is ready for the training. 

## Preprocessing Notes

- The data preprocessing works for both Sockeye and Fairseq.
- Make sure you use parallel data, otherwise you will receive errors.
- Please, execute the bash script as soon as you start one of the instructions to replicate one of the three models mentioned above.
- The Datasets from the bash script have a size of almost 8.2 GB. Make sure you download the datasets only one time for all three models.
- You can find more preprocessing examples with the Moses Tokenizer and BPE in the [Sockeye Tutorials](https://awslabs.github.io/sockeye/tutorials.html) and [FairSeq Examples](https://github.com/pytorch/fairseq/tree/main/examples/translation#). 
- Please, take also a look of the bash scripts in the FairSeq link above, if you want to know how to process *.sgm data.


# Levenshtein

To use Levenshtein for translation, go to section [Translate Levenshtein](#markdown-header-translate-levenshtein). If you want to train your own Levenshtein model, go to section [Train Levenshtein](#markdown-train-levenshtein).

## Translate Levenshtein

You can translate own text inputs with `echo -e`. Keep in mind you always have to add the flag `-e` to `echo`. 
To include constraints, use the tab delimiter with `|||` and your desired German term.
FairSeq already uses the moses tokenizer and apply Byte-Pair-Encoding (BPE) on your input.

	```
	echo -e " The older light systems consume around 100 Watts.\t|||verbrauchen" \
	| python interactive_with_constraints.py \
		data-bin/en-de-levT \
		-s en -t de \
		--task translation_lev \
		--path checkpoints/checkpoint_best.pt \
		--iter-decode-max-iter 9 \
		--iter-decode-eos-penalty 0 \
    	--tokenizer moses \		
    	--bpe fastbpe \
    	--bpe-codes data-bin/const_levt_en_de/ende.code \
    	--remove-bpe \		
		--print-step \
		--batch-size 400 \
		--buffer-size 4000 \
		--preserve-constraint | tee /tmp/gen.out
	```
	
If you wish to translate own files, follow these steps:

	```
		cat input_test.en \
		| python interactive_with_constraints.py \
			data-bin/const_levt_en_de \
			-s en -t de \
			--task translation_lev \
			--path data-bin/const_levt_en_de/checkpoint_best.pt \
			--iter-decode-max-iter 9 \
			--iter-decode-eos-penalty 0 \
			--tokenizer moses \
			--bpe fastbpe \
			--bpe-codes data-bin/const_levt_en_de/ende.code \
			--remove-bpe \
			--print-step \
			--batch-size 400 \
			--buffer-size 4000 \
			--preserve-constraint | tee /tmp/gen.out
	```
	
after that

	```
		cat /tmp/gen.out \
			| grep ^H \
			| sed 's/^H\-//' \
			| sort -n -k 1 \
			| cut -f 3 > output_test.de
	```

## Train Levenshtein 

After preprocessing the data, you need the - [Constrained-LevT Repository](https://github.com/raymondhs/constrained-levt). 
It contains the older version of FairSeq (2020). Newer versions of FairSeq are not compatible with the scripts.
If you installed a newer version you can re-install it. Make sure you work within virtual environments.
Let's start to train a English to German Levenshtein transformer model.

__Note:__ If you want to train the Levenshtein model in the same way like Susanto et al did, you need _knowledge distillation_ ([Understanding Knowledge Distillation in Non-autoregressive Machine Translation (Zhou et al., 2021)](https://arxiv.org/abs/1911.02727)).
This section shows how to train the Levenshtein model without knowledge distillation. See [Non-autoregressive Neural Machine Translation (NAT)
](https://github.com/pytorch/fairseq/blob/main/examples/nonautoregressive_translation/README.md) for more information.

1. You have to clone and install the *Constrained-LevT* repo with its own FairSeq version.

	```
	git clone https://github.com/raymondhs/constrained-levt
	cd constrained-levt
	pip install --editable .
	```

2. Move the training data or in this case the *dataset* inside of the *constrained-levt* repo.

	`mv -rv ../dataset ../constrained-levt/`
	
3. You have to binarize the dataset. Make sure you add the `--joined-dictionary` parameter.

	```
	fairseq-preprocess --source-lang en --target-lang de \
    --trainpref dataset/train --validpref dataset/valid \
    --destdir data-bin/en-de-levT \
    --workers 20 \ 
    --joined-dictionary

	```
	
4. Now the Levenshtein model can be trained.

	```
	CUDA_VISIBLE_DEVICES=0 fairseq-train \
    data-bin/en-de-levT \
    --save-dir checkpoints \
    --encoder-learned-pos \
    --decoder-learned-pos \
    --encoder-attention-heads 8 \
    --decoder-attention-heads 8 \
    --encoder-embed-dim 512 \
    --decoder-embed-dim 512 \
    --encoder-ffn-embed-dim 2048 \
    --decoder-ffn-embed-dim 2048 \
    --ddp-backend=c10d \
    --task translation_lev \
    --criterion nat_loss \
    --arch levenshtein_transformer \
    --noise random_delete \
    --share-all-embeddings \
    --optimizer adam --adam-betas '(0.9,0.98)' \
    --lr 0.00005 --lr-scheduler inverse_sqrt \
    --min-lr '1e-09' --warmup-updates 10000 \
    --warmup-init-lr '1e-07' --label-smoothing 0.1 \
    --dropout 0.5 --weight-decay 0.01 \
    --encoder-layers 6 \
    --decoder-layers 6 \
    --apply-bert-init \
    --log-format 'simple' --log-interval 10 \
    --fixed-validation-seed 7 \
    --max-tokens 10000 \
    --save-interval-updates 10000 \
    --max-update 300000 \
    --fp16 \
    --no-epoch-checkpoints
	```
	
6. Note that you can change the learning rate, max-tokens, batch-size and max-updates any time.

7. Now the model can be also used to translate binarized inputs.

	```
	fairseq-generate \
		data-bin/en-de-levT \
		--gen-subset test \
		--task translation_lev \
		--path checkpoints/checkpoint_best.pt \
		--iter-decode-max-iter 9 \
		--iter-decode-eos-penalty 0 \
		--remove-bpe \
		--print-step \
		--batch-size 400
	```
	
8. Congratulations! You have trained a functional Levenshtein transformer model!

# Vectorized Dynamic Beam Allocation

To use Vectorized Dynamic Beam Allocation for translation, go to section [Translate with Vectorized Dynamic Beam Allocation](#markdown-header-translate-with-vectorized-dynamic-beam-allocation). If you want to train your own model, go to section [Train with Vectorized Dynamic Beam Allocation](#markdown-train-with-vectorized-dynamic-beam-allocation).

## Translate with Vectorized Dynamic Beam Allocation

You can translate own text inputs with `echo -e`. Keep in mind you always have to add the flag `-e` to `echo`. 
To include constraints, use the tab delimiter and append your desired German term.
FairSeq already uses the moses tokenizer and apply Byte-Pair-Encoding (BPE) on your input.
The beam size is currently 5.

	```
	echo -e "Machine translation is hard to influence.\tschwer\terfassen" \
	| fairseq-interactive data-bin/wmt17_en_de/ \
		 --path checkpoints/checkpoint_best.pt \
 		 --tokenizer moses \		 
		 --bpe fastbpe \
		 --bpe-codes examples/translation/wmt17_en_de/code \
		 --constraints ordered \
		 -s en -t de \
		 --beam 5
	```
If you wish to translate own files, follow these steps:

```
	cat input_test.en \
	| fairseq-interactive . \
	  --path model1.pt \
	  --tokenizer moses \
	  --bpe fastbpe \
	  --bpe-codes bpecodes \
	  --remove-bpe \
	  --constraints ordered \
	  -s en -t de \
	  --beam 5 | tee /tmp/gen.out
```
After that

```
	cat /tmp/gen.out \
	| grep ^H \
	| sed 's/^H\-//' \
	| sort -n -k 1 \
	| cut -f 3 > output_test.de
```

## Train Vectorized Dynamic Beam Allocation 

Similar to the Levenshtein training we need to clone the latest FairSeq repository and install its components.
A English to German transformer model will be trained. Make sure to train within a virtual environment.

1. Clone and install the FairSeq 

	```
		git clone https://github.com/pytorch/fairseq
		cd fairseq
		pip install --editable ./
	```
	
2. Move the training data or in this case the _dataset_ inside of the _constrained-levt_ repo.

	`mv -rv ../dataset ../fairseq/`
	
3. You have to binarize the dataset. 

	```
	fairseq-preprocess --source-lang en --target-lang de \
    --trainpref dataset/train --validpref dataset/valid \
	--testpref dataset/test \
    --destdir data-bin/en-de-vdba \
    --workers 20 \ 

	```
	
4. Now you can train the model.

	```
	CUDA_VISIBLE_DEVICES=0 fairseq-train \
		data-bin/en-de-vdba \
		--save-dir checkpoints \
		--arch transformer_wmt_en_de --share-decoder-input-output-embed \
		--optimizer adam --adam-betas '(0.9, 0.98)' --clip-norm 0.0 \
		--lr 5e-4 --lr-scheduler inverse_sqrt --warmup-updates 4000 \
		--dropout 0.3 --weight-decay 0.0001 \
		--criterion label_smoothed_cross_entropy --label-smoothing 0.1 \
		--task translation \
		--encoder-layers 6 \
		--decoder-layers 6 \
		--max-tokens 4096 \
		--batch-size 80 \
		--log-format 'simple' --log-interval 10 \
		--no-epoch-checkpoints
		--fp16
	```

5. Congratulations! You have trained a transformer model which supports VDBA.


# Terminology Constraints

To use Terminology Constraints for translation, go to section [Translate Terminology Constraint](#markdown-header-translate-terminology-constraint). If you want to train your own model, go to section [Train Terminology Constraint Model](#markdown-train-terminology-constraint-model).

## Translate Terminology Constraint

You can translate own text inputs. 
For Sockeye you have to tokenize and apply your text first. You can use the `tok_and_BPE.sh code` script.
The input must be annotated with source factors: 0 for the default terms, 1 for the source terms and 2 for the target terms.

```
echo "I|0 write|0 my|0 thesis|1 Absch@@|2 luss@@|2 arbeit|2" \
  | sockeye-translate -m your_model 2>/dev/null \
  | sed -r 's/@@( |$)//g'
```

You can also translate this input with its source factors in a file. 

```
cat test.en \
  | sockeye-translate -m your_model 2>/dev/null \
  | sed -r 's/@@( |$)//g'
```

If you have a file with the line

`I write my thesis Absch@@ luss@@ arbeit`

you can also create a corresponding token parallel file including the following line

`0 0 0 1 2 2 2`

and you can translate it:

```
	sockeye-translate \
		--input input_test.en \
		--input-factors input_test_sf.en \
		--output output \
		--model your_model \
		--dtype float16 \
		--beam-size 5 \
		--batch-size 64
```

The translation for the baseline model doesn't support source factoring. You just need to translate in this way.


```
	sockeye-translate \
		--input input_test.en \
		--output output \
		--model your_model \
		--dtype float16 \
		--beam-size 5 \
		--batch-size 64
```

## Train Terminology Constraint Model

1. You need to annotate about 10% lines of the train and valid files first. 
   In the train.en and valid.en add you desired German target term after its corresponding English source term.

`I write my thesis Abschlussarbeit`

2. In the train.de and valid.de you also need to include the desired target terms

`Ich schreibe meine These` becomes `Ich schreibe meine Abschlussarbeit`.

3. Apply Byte-Pair-Encoding on all the train and valid files. You can use the `tok_and_BPE.sh` script with the corresponding BPE code file.

4. Make copies of the train.en and valid.en files. The source and target terms need to be replaced. Replace all source terms with 1 and all 
   target terms with 2. Everything else must be replace with 0.
	
5. Preprocess the training data and their corresponding source factoring file.

	```
	sockeye-prepare-data \
    -s t_train.en \
    -t t_train.de --shared-vocab \
    -sf s_train.en \
    --source-factors-use-source-vocab true \
    --word-min-count 2 --pad-vocab-to-multiple-of 8 --max-seq-len 95 \
    --num-samples-per-shard 10000000 --output prepared --max-processes $(nproc)
	```
	
6. The training with the source factoring can be started.

	```
	sockeye-train -d prepared \
	-vs t_valid.en \
	-vt t_valid.de \
	--shared-vocab \
	-o term_constraint_model \
	--min-num-epochs 50 \
	--max-num-epochs 100 \
	--batch-size 560 \
	--transformer-attention-heads 8:8 \
	--transformer-activation-type 'relu':'relu' \
	--transformer-dropout-act 0.1:0.1 \
	--transformer-dropout-attention 0.1:0.1 \
	--transformer-dropout-prepost 0.1:0.1 \
	--transformer-feed-forward-num-hidden 2048:2048 \
	--transformer-model-size 512 --num-layers 2:2 \
	--transformer-positional-embedding-type fixed \
	--transformer-preprocess n:n \
	--transformer-postprocess dr:dr \
	--source-factors-num-embed 16 \
	--dtype float32 \
	--max-seq-len 101:101 \
	--num-words 32302:32302 \
	--num-embed 512:512 \
	--label-smoothing 0.1 \
	--embed-dropout 0.0:0.0 \
	--loss cross-entropy \
	--keep-last-params 1 \
	--cache-last-best-params 1 \
	--validation-source-factors s_valid.en
	```
__NOTE:__ the parameters `--keep-last-params` and `--cache-last-best-params` keep only the best parameter. If the model needs to be retrained, add the parameter `--overwrite-output`.

# Evaluation
You can calculate the BLEU, TER and chrf2 values with the line
```
	sacrebleu reference_dataset_finalised.de -i output_sockeye_dataset_constraints_finalised.en -m bleu chrf ter
```

For more information about SacreBLEU see [here](https://www.cs.cmu.edu/~alavie/METEOR/README.html).

For calculating the Meteor score you need to download the Meteor Java [files](http://www.cs.cmu.edu/~alavie/METEOR/index.html#Download).
You need at least ``openjdk version "15" 2020-09-15``

The Meteor score is calculated with the following line.

```
	java -Xmx2G -jar meteor-1.5.jar output_sockeye_dataset_constraints_finalised.en reference_dataset_finalised.de -l de -norm
```

# Updates

This section lists the latest updates of this repository:

**(1.0.3 - 11/04/2022)**

- Added folder structure with translation samples, training-data samples and 'translation_scores.xlsx'
- Added 'outdated_scripts'
- Added 'bash_scripts'
- Moved bash scripts to 'bash_scripts'
- Update README.md

**(1.0.2 - 10/02/2022)**

- Replaced 'normalize.py' and 'tok.py'
- Adjust '--min-num-epochs' and '--max-num-epochs parameters in 'Train Terminology Constraints' point 6
- Spell checking in 'README.md'

**(1.0.1 - 09/02/2022)**

- Added files 'sed_repl.sh' and 'insertion.sh' in the root
- Added folder 'model_translation' with data used for the models
- Included instruction to train and translate Terminology Constraints for Sockeye
- Spell checking in README.md
- Code correction in README.md


# Notes

- Dataset have a size of 8.2 GB.
- Sockeye 3 no longer supports the MXNet library, only PyTorch. 
- The folder 'translation_samples' contains all necessary files for the evaluation
- You find bash scripts for the translation and training in 'bash_scripts'

# Further Reading

- [The Sockeye 2 Neural Machine Translation Toolkit at AMTA 2020 (Domhan et al., 2020)](https://arxiv.org/abs/2008.04885)
- [Levenshtein Transformer (Gu et al., 2019)](https://arxiv.org/abs/1905.11006)
- [fairseq: A Fast, Extensible Toolkit for Sequence Modeling (Ott et al., 2019)](https://arxiv.org/abs/1904.01038)
- [Non-Autoregressive Neural Machine Translation (Gu et al., 2018)](https://arxiv.org/abs/1711.02281)
- [Understanding Knowledge Distillation in Non-autoregressive Machine Translation (Zhou et al., 2021)](https://arxiv.org/abs/1911.02727)
- [Fast Lexically Constrained Decoding with Dynamic Beam Allocation for Neural Machine Translation (Post and Vilar, 2018)](https://arxiv.org/abs/1804.06609)
- [Neural Machine Translation Decoding with Terminology Constraints (Hasler et al., 2018)](https://aclanthology.org/N18-2081/)
- [Lexically Constrained Decoding for Sequence Generation Using Grid Beam Search (Hokamp and Liu, 2017)](https://aclanthology.org/P17-1141/)
- [Sockeye: A Toolkit for Neural Machine Translation (Hieber et al., 2017)](https://arxiv.org/abs/1712.05690)
- [Attention is all you need (Vaswani et al., 2017)](https://arxiv.org/abs/1706.03762)
- [Linguistic Input Features Improve Neural Machine Translation (Sennrich and Haddow, 2016)](https://arxiv.org/abs/1606.02892)
- [Neural Machine Translation of Rare Words with Subword Units (Sennrich et al., 2015)](https://arxiv.org/abs/1508.07909)
- [Moses: Open Source Toolkit for Statistical Machine Translation (Koehn et al, 2007)](https://aclanthology.org/P07-2045/)
- [sacreBLEU](https://www.cs.cmu.edu/~alavie/METEOR/README.html)

# References

- [PyTorch Tutorial](https://pytorch.org/tutorials/)
- [AWS Sockeye Repository](https://github.com/awslabs/sockeye)
- [AWS Sockeye Homepage](https://awslabs.github.io/sockeye/)
- [fairseq Repository](https://github.com/pytorch/fairseq)
- [Constrained-LevT Repository](https://github.com/raymondhs/constrained-levt)
- [Meteor 1.5: Automatic Machine Translation Evaluation System](https://github.com/mjpost/sacreBLEU)

